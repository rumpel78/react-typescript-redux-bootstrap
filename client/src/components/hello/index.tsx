// src/components/Hello.tsx

import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { StoreState } from 'src/types';
import * as actions from '../../actions';
import './style.css';

interface HelloReduxProps {
    language: string;
    enthusiasmLevel?: number;
    onIncrement?: () => void;
    onDecrement?: () => void;
}

interface HelloOwnProps {
    username: string;
}

type HelloProps = HelloOwnProps & HelloReduxProps;

const HelloBase = ({ username, language, enthusiasmLevel = 1, onIncrement, onDecrement }: HelloProps) => {
    if (enthusiasmLevel <= 0) {
        throw new Error('You could be a little more enthusiastic. :D');
    }

    return (
        <div className="hello">
            <div className="greeting">
                Hello {username + ' using ' + language + getExclamationMarks(enthusiasmLevel)}
            </div>
            <div>
                <button onClick={onDecrement}>-</button>
                <button onClick={onIncrement}>+</button>
            </div>
        </div>
    );
};

// helpers
function getExclamationMarks(numChars: number) {
    return Array(numChars + 1).join('!');
}

const mapStateToProps = (state: StoreState, ownProps: HelloOwnProps) => {
    return {
        enthusiasmLevel: state.enthusiasmLevel,
        language: state.languageName,
    };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.EnthusiasmAction>) => {
    return {
        onIncrement: () => dispatch(actions.incrementEnthusiasm()),
        onDecrement: () => dispatch(actions.decrementEnthusiasm())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HelloBase);
